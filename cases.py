#specific tests
import gc

from keras.applications import InceptionV3
from keras.utils import to_categorical

from keras import Model, Input
from keras.models import Sequential
from keras.layers import Activation, Dropout, Flatten, Dense

from defs import *

import models as m
import utils as u
import generators as gen

func_h_lst = [gen.bgronly, gen.bgr2hsv, gen.bgr2lab]
func_h_name_lst = ['RBG', 'HSV', 'LAB']


def use_pre_trained_model_weather():
    base_model = u.get_pre_model()
    base_model.pop()
    base_model.pop()
    base_model.pop()
    for layer in base_model.layers:
        layer.trainable = False
    last_layer = base_model.get_layer('conv2d_5')
    last_output = last_layer.output
    x = Flatten()(last_output)
    x = Dense(512, activation='relu')(x)
    x = Dense(4, activation='softmax')(x)

    model = Model(base_model.input, x)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    t_gen, v_gen = gen.gen_multi_generator('data', 128, None, True)

    model.fit_generator(
        t_gen,
        steps_per_epoch=15,
        epochs=5,
        validation_data=v_gen,
        validation_steps=8, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'Weather_pretrained', True)


def use_pre_trained_model_v3():
    model = Sequential()
    model.add(InceptionV3(include_top=False, input_shape=(IMAGE_WIDTH, IMAGE_HEIGHT, 3), weights = 'imagenet'))
    for layer in model.layers:
        layer.trainable = False
    model.add(Flatten())

    model.add(Dense(4, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    t_gen, v_gen = gen.get_data_all_train(0, 0, 0)
    model.fit(t_gen, to_categorical(v_gen), validation_split=0.1, epochs=12, batch_size=256, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'InceptionV3', True)


def binary_hist(paths, test_paths, keys, func, name):

    train_images, train_labels = gen.get_data_hist_binary(gen.bgronly, paths)
    model = Sequential()
    model.add(Dense(512, input_dim=60*60*60, activation='relu'))
    model.add(Dense(512, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    print(model.summary())

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])
    model.fit(
        train_images,
        train_labels,
        epochs=5,
        batch_size=128, validation_split=0.15, class_weight='balanced')
    u.check_predictions_binary(model, test_paths ,gen.get_data_hist_binary, func, name, keys)

def temp_hist_hsv():
    binary_hist(paths_temp, paths_temp_test, u.dict_temp, gen.bgr2hsv, "temperature_hsv ")

def lumi_hist_hsv():
    binary_hist(paths_lumi, paths_lumi_test, u.dict_lumi, gen.bgr2hsv, "brigtness_hsv")

def temp_hist_rgb():
    binary_hist(paths_temp, paths_temp_test, u.dict_temp, gen.bgr2hsv, "temperature_rgb ")

def lumi_hist_rgb():
    binary_hist(paths_lumi, paths_lumi_test, u.dict_lumi, gen.bgr2hsv, "brigtness_rgb")

def test_hist_all(ind):
    func_h = func_h_lst[ind]
    func_h_name = func_h_name_lst[ind]

    train_images, train_labels = gen.get_data_hist_all(func_h)
    model = Sequential()
    model.add(Dense(1024, input_dim=60*60*60, activation='relu'))
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(4, activation='softmax'))

    train_labels_one_hot = to_categorical(train_labels)

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    history = model.fit(
        train_images,
        train_labels_one_hot,
        epochs=6,
        batch_size=128, validation_split=0.15, class_weight='balanced'
    )
    del train_images, train_labels
    gc.collect()
    u.check_predictions(model, common_path+'data_test', gen.get_data_hist_all_test, func_h, func_h_name, u.dict_all)


def use_simple_model():
    t_gen, v_gen = gen.gen_multi_generator('data', 10, rotate=True)
    model = m.simple_model()
    model.fit_generator(
        t_gen,
        steps_per_epoch=200,
        epochs=5,
        validation_data=v_gen,
        validation_steps=50, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'Simple_model_gen', u.dict_all)


def use_simple_model_no_gen():
    t_gen, v_gen = gen.get_data_all_train(0, 0, 0)
    model = m.simple_model()
    model.fit(t_gen, to_categorical(v_gen), validation_split=0.1, epochs=10, batch_size=128, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'Simple_model', u.dict_all)

def use_colornet():
    t_gen, v_gen = gen.gen_multi_generator('data', 10, rotate=True)
    model = m.simple_model()
    model.fit_generator(
        t_gen,
        steps_per_epoch=200,
        epochs=5,
        validation_data=v_gen,
        validation_steps=50, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'Colornet_gen', u.dict_all)


def use_colornet_no_gen():
    t_gen, v_gen = gen.get_data_all_train(0, 0, 0)
    model = m.color_net(4)
    model.fit(t_gen, to_categorical(v_gen), validation_split=0.1, epochs=5, batch_size=128, class_weight='balanced')
    u.check_predictions(model, 'data_test', gen.get_data_all_test, None, 'Colornet', u.dict_all)
