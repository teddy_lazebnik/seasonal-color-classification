#taken from
#https://towardsdatascience.com/building-convolutional-neural-networks-in-python-using-keras-7e4652f6456f

import os
import gc
import cv2
import random
import utils as u

import generators as gen
import numpy as np
import matplotlib.pyplot as plt
from keras import callbacks
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras_preprocessing.image import ImageDataGenerator

from defs import *




# 1.) Get all image path sets
cloud_set = ['/home/alona/PycharmProjects/finalll/data_pretr_ain/class_cloudy/'+i for i in os.listdir('/home/alona/PycharmProjects/finalll/data_pretr_ain/class_cloudy/')]
rain_set = ['/home/alona/PycharmProjects/finalll/data_pretr_ain/class_rain/'+i for i in os.listdir('/home/alona/PycharmProjects/finalll/data_pretr_ain/class_rain')]
sunrise_set = ['/home/alona/PycharmProjects/finalll/data_pretr_ain/class_sunrise/'+i for i in os.listdir('/home/alona/PycharmProjects/finalll/data_pretr_ain/class_sunrise')]
sunshine_set = ['/home/alona/PycharmProjects/finalll/data_pretr_ain/class_shine/'+i for i in os.listdir('/home/alona/PycharmProjects/finalll/data_pretr_ain/class_shine')]

# 2.) Randomly Shuffle Images Before Splitting for Training and Testing
random.shuffle(cloud_set)
random.shuffle(rain_set)
random.shuffle(sunrise_set)
random.shuffle(sunshine_set)

# 3.) Training and Testing Image Sets
train_set = cloud_set[:150] + rain_set[:150] + sunrise_set[:150] + sunshine_set[:150]
test_set = cloud_set[150:] + rain_set[150:] + sunrise_set[:150] + sunshine_set[:150]

# 4.) Garbage Collection
del cloud_set, rain_set, sunrise_set, sunshine_set
gc.collect()

# 5.) Image Pre-Processing
nRows = 150  # Width
nCols = 150  # Height
channels = 3  # Color Channels RGB-3, Grayscale-1

# 6.) Training and Testing Set Labeling
X_train = []
X_test = []
y_train = []
y_test = []

# 7.) Read and Label Each Image in the Training Set
for image in train_set:
    try:
        X_train.append(cv2.resize(cv2.imread(image, cv2.IMREAD_COLOR), (nRows, nCols), interpolation=cv2.INTER_CUBIC))
        if 'cloudy' in image:
            y_train.append(1)
        elif 'rain' in image:
            y_train.append(2)
        elif 'sunrise' in image:
            y_train.append(3)
        elif 'shine' in image:
            y_train.append(4)
    except Exception:
        print('Failed to format: ', image)

# 8.) Read and Label Each Image in the Testing Set
for image in test_set:
    try:
        X_test.append(cv2.resize(cv2.imread(image, cv2.IMREAD_COLOR), (nRows, nCols), interpolation=cv2.INTER_CUBIC))
        if 'cloudy' in image:
            y_test.append(1)
        elif 'rain' in image:
            y_test.append(2)
        elif 'sunrise' in image:
            y_test.append(3)
        elif 'shine' in image:
            y_test.append(4)
    except Exception:
        print('Failed to format: ', image)

# 9.) Garbage Collection
del train_set, test_set
gc.collect()

# 10.) Convert to Numpy Arrays
X_train = np.array(X_train)
X_test = np.array(X_test)
y_train = np.array(y_train)
y_test = np.array(y_test)

# 11.) Switch Targets to Categorical
print(y_train)
print(y_test)
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# 12.) Convolutional Neural Network
model = Sequential()
model.add(Conv2D(32, kernel_size=3, activation='relu', input_shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 3)))
model.add(MaxPooling2D(2, 2))
model.add(Conv2D(64, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Dropout(0.9))
model.add(Conv2D(128, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Conv2D(256, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Dropout(0.9))
model.add(Conv2D(512, kernel_size=3, activation='relu'))
model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(4, activation='softmax'))

# 13.) Model Summary
print(model.summary())

# 14.) Compile and Train the Model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

train_generator, test_generator = gen.gen_gen()

history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)

loss, accuracy = model.evaluate_generator(test_generator, 50)

YNew = model.predict(u.create_test_data())
print(YNew)

print('Accuracy: %.2f, loss: %2.f' % (accuracy * 100, loss))

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights('pre__trained.h5')  # always save your weights after training or during training
print('loss ' +str(loss))
print('accuracy '+str(accuracy))

# 15.) Plot Accuracy Over Training Period
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# 16.) Plot Loss Over Training Period
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('a.png')