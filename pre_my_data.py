import os
import gc
import cv2
import random
import numpy as np
import matplotlib.pyplot as plt
from keras import callbacks
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras_preprocessing.image import ImageDataGenerator

from defs import *
import generators as gen

# 12.) Convolutional Neural Network
model = Sequential()
model.add(Conv2D(32, kernel_size=3, activation='relu', input_shape=(IMAGE_WIDTH, IMAGE_HEIGHT,  3)))
model.add(MaxPooling2D(2, 2))
model.add(Conv2D(64, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Conv2D(128, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Conv2D(256, kernel_size=3, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(4, activation='softmax'))

# 13.) Model Summary
print(model.summary())

# 14.) Compile and Train the Model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
history = earlystopping = callbacks.EarlyStopping(monitor="val_loss",
                                            mode="min", patience=5,
                                            restore_best_weights=True)

train_generator, test_generator = gen.gen_multi_generator('data', 128, None, True)

history = model.fit_generator(
        train_generator,
        steps_per_epoch=10,
        epochs=10,
        validation_data=test_generator,
        validation_steps=3, callbacks =[earlystopping], class_weight='balanced')

loss, accuracy = model.evaluate_generator(test_generator, 25)

print('Accuracy: %.2f, loss: %2.f' % (accuracy * 100, loss))

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights('pre__trained.h5')  # always save your weights after training or during training
print('loss ' +str(loss))
print('accuracy '+str(accuracy))

