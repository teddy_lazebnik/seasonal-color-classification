#data generation\reading functions

import os
import random
import cv2
from keras_preprocessing.image import ImageDataGenerator, np

from defs import *


def get_generator_binary(dir, batch_size, proc_func=None):
    datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.3,
        width_shift_range=0.2,
        height_shift_range=0.2,
        zoom_range=0.2,
        rotation_range=0.2,
        validation_split=0.2, preprocessing_function=proc_func)

    train_generator = datagen.flow_from_directory(
        dir,
        target_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
        batch_size=batch_size,
        class_mode='binary', subset='training')

    validation_generator = datagen.flow_from_directory(
        dir,
        target_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
        batch_size=batch_size,
        class_mode='binary', subset='validation')

    return train_generator, validation_generator

def gen_multi_generator(path, batch_size, proc_func=None, rotate=False):
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.3,
        validation_split=0.2)

    if rotate:
        train_generator = train_datagen.flow_from_directory(
             common_path+path,
            batch_size=batch_size,
            target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
            class_mode='categorical', subset='training')

        test_generator = train_datagen.flow_from_directory(
            common_path + path,
            batch_size=batch_size,
            target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
            class_mode='categorical', subset='validation')
    else:
        train_generator = train_datagen.flow_from_directory(
            common_path + path,
            batch_size=batch_size,
            target_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
            class_mode='categorical', subset='training')

        test_generator = train_datagen.flow_from_directory(
            common_path + path,
            batch_size=batch_size,
            target_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
            class_mode='categorical', subset='validation')

    return train_generator, test_generator

def get_data_all_train(d1, d2, d3):
    data = []
    tags = []
    tag_names = []
    paths = [common_path + 'data/class_' + name for name in names]
    for i in range(len(paths)):
        for file in (os.listdir(paths[i])):
            x = cv2.imread(paths[i]+"/"+file)
            data.append(x)
            tags.append(i)
            tag_names.append(paths[i]+"/"+file)
    united = list(zip(data, tags, tag_names))

    random.shuffle(united)

    data, tags, tag_names = zip(*united)

    return np.asarray(data), np.asarray(tags)

def get_data_all_test(d1, d2, d3):
    data = []
    tags = []
    tag_names = []
    paths = [common_path + 'data_test/class_' + name for name in names]
    for i in range(len(paths)):
        for file in (os.listdir(paths[i])):
            x = cv2.imread(paths[i]+"/"+file)
            data.append(x)
            tags.append(i)
            tag_names.append(paths[i]+"/"+file)
    united = list(zip(data, tags, tag_names))

    random.shuffle(united)

    data, tags, tag_names = zip(*united)

    return np.asarray(data), np.asarray(tags)


def get_data_hist_all(hist_func):
    data = []
    tags = []
    paths = [common_path + 'data/class_' + name for name in names]
    for i in range(len(paths)):
        for file in (os.listdir(paths[i])):
            x = cv2.imread(paths[i]+"/"+file)
            hist = hist_func(x)
            hist = hist.flatten()
            cv2.normalize(hist, hist, 0, 1, cv2.NORM_MINMAX, -1)
            data.append(hist)
            tags.append(i)
    united = list(zip(data, tags))
    random.shuffle(united)
    data, tags = zip(*united)
    return np.asarray(data), np.asarray(tags)


def get_data_hist_all_test(hist_func, d1, d2):
    data = []
    tags = []
    paths = [common_path + 'data_test/class_' + name for name in names]
    for i in range(len(paths)):
        for file in (os.listdir(paths[i])):
            x = cv2.imread(paths[i]+"/"+file)
            hist = hist_func(x)
            hist = hist.flatten()
            cv2.normalize(hist, hist, 0, 1, cv2.NORM_MINMAX, -1)
            data.append(hist)
            tags.append(i)
    united = list(zip(data, tags))

    random.shuffle(united)

    data, tags = zip(*united)

    return np.asarray(data), np.asarray(tags)


def get_data_hist_binary(hist_func, paths, d1=None):
    data = []
    tags = []
    for i in range(len(paths)):
        for file in (os.listdir(paths[i])):
            x = cv2.imread(paths[i]+"/"+file)
            hist = hist_func(x)
            hist = hist.flatten()
            cv2.normalize(hist, hist, 0, 1, cv2.NORM_MINMAX, -1)
            data.append(hist)
            tags.append(i)
    united = list(zip(data, tags))
    random.shuffle(united)
    data, tags = zip(*united)
    return np.asarray(data), np.asarray(tags)


def bgronly(img):
    hist = cv2.calcHist([img], [0, 1, 2], None, [60, 60, 60], [0, 255, 0, 255, 0, 255])
    return hist

def bgr2lab(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    hist = cv2.calcHist([img], [0, 1, 2], None, [60, 60, 60], [0, 255, 0, 255, 0, 255])
    return hist

def bgr2hsv(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV_FULL)
    hist = cv2.calcHist([img], [0, 1, 2], None, [60, 60, 60], [0, 180, 0, 255, 0, 255])
    return hist