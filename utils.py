#common utilties

import os

import cv2
import numpy as np
from keras.engine.saving import model_from_json
from keras.utils import to_categorical
from sklearn.metrics import confusion_matrix
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt

from defs import *

dict_all = dict()
dict_all[0] = 'Autumn'
dict_all[1] = 'Spring'
dict_all[2] = 'Summer'
dict_all[3] = 'Winter'

dict_temp = dict()
dict_temp[0] = 'cold'
dict_temp[1] = 'warm'

dict_lumi = dict()
dict_lumi[0] = 'bright'
dict_lumi[1] = 'muted'


def get_pre_model():
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights("pre__trained.h5")
    return loaded_model


def init_data_sep_aux(dir_name_old, dir_name_new, name):
    count = 1
    for file in (os.listdir(dir_name_old)):
        x = cv2.imread(dir_name_old + "/" + file)
        height, width, dim = x.shape
        crop_img1 = x[0:int(height / 8), :]
        crop_img2 = x[int(height * 0.4):int(height * 0.7), :]
        crop_img3 = x[int(8 * height / 9):height - 1, :]
        x = np.vstack((crop_img1, crop_img2, crop_img3))
        x = cv2.resize(x, (IMAGE_HEIGHT, IMAGE_WIDTH), interpolation=cv2.INTER_AREA)
        cv2.imwrite(dir_name_new + '/' + name + '_image_' + str(count) + '.png', x)
        count += 1


def init_data_sep():
    os.mkdir('/home/alona/PycharmProjects/finalll/data_temp/')
    os.mkdir('/home/alona/PycharmProjects/finalll/data_lumi/')
    os.mkdir('/home/alona/PycharmProjects/finalll/data_temp_test/')
    os.mkdir('/home/alona/PycharmProjects/finalll/data_lumi_test/')
    temp = ['cold', 'warm']
    bright = ['bright', 'muted']
    for name in temp:
        for x in ['', '_test']:
            dir_name_new = '/home/alona/PycharmProjects/finalll/data_temp'+x+'/class_'+name
            os.mkdir(dir_name_new)
            dir_name_old = '/home/alona/data/aft3'+x+'/temp/'+name
            init_data_sep_aux(dir_name_old, dir_name_new, name)

    for name in bright:
        for x in ['', '_test']:
            dir_name_new = '/home/alona/PycharmProjects/finalll/data_lumi'+x+'/class_'+name
            os.mkdir(dir_name_new)
            dir_name_old = '/home/alona/data/aft3'+x+"/lum/"+name
            init_data_sep_aux(dir_name_old, dir_name_new, name)


def init_data():
    os.mkdir('/home/alona/PycharmProjects/finalll/data/')
    os.mkdir('/home/alona/PycharmProjects/finalll/data_test/')
    for name in names:
        for string in ['', '_test']:
            dir_name_new = '/home/alona/PycharmProjects/finalll/data'+string+'/class_'+name
            os.mkdir(dir_name_new)
            dir_name_old = '/home/alona/data/aft2'+string+'/'+name
            count = 1
            for file in (os.listdir(dir_name_old)):
                x = cv2.imread(dir_name_old+"/"+file)
                height, width, dim = x.shape
                crop_img1 = x[0:int(height/8), :]
                crop_img2 = x[int(height*0.4):int(height*0.7), :]
                crop_img3 = x[int(8*height/9):height-1, :]
                x = np.vstack((crop_img1, crop_img2, crop_img3))
                x = cv2.resize(x, (IMAGE_HEIGHT, IMAGE_WIDTH), interpolation=cv2.INTER_AREA)
                cv2.imwrite(dir_name_new+'/'+name+'_image_'+str(count)+'.png', x)
                count+=1


def init_data_pre():
    os.mkdir('/home/alona/PycharmProjects/finalll/data_pretrain/')
    for name in names_ar:
        dir_name_new = '/home/alona/PycharmProjects/finalll/data_pretrain/class_'+name
        os.mkdir(dir_name_new)
        dir_name_old = '/home/alona/PycharmProjects/finalll/dataset2/class_'+name
        count = 1
        for file in (os.listdir(dir_name_old)):
            x = cv2.imread(dir_name_old+"/"+file)
            cv2.imwrite(dir_name_new+'/'+name+'_image_'+str(count)+'.png', x)
            count+=1


def check_predictions(model, pred_path, gen_func, add_func, add_text, mapping, rotate=False):
    test_im, test_labels = gen_func(add_func, pred_path, rotate)
    _, accuracy = model.evaluate(test_im, to_categorical(test_labels))
    acc = (accuracy * 100)

    predictions = model.predict(test_im)
    predictions = np.argmax(predictions, axis=1)


    predictions_list  = predictions.tolist()
    true_list = test_labels.tolist()
    for l in [predictions_list, true_list]:
        for i in range(len(l)):
            l[i] = mapping[l[i]]

    mat = confusion_matrix(true_list, predictions_list, labels=list(mapping.values()))
    df_cm = pd.DataFrame(mat, columns=np.unique(true_list), index=np.unique(true_list))
    df_cm.index.name = 'Actual'
    df_cm.columns.name = 'Predicted'
    plt.figure(figsize=(8, 5))
    sn.set(font_scale=1.1)
    sn.heatmap(df_cm, cmap='Blues', annot=True, annot_kws={"size": 12}, cbar=False)
    plt.title(add_text+ ", accuracy: "+str(acc))
    plt.savefig(add_text+".png")
    plt.clf()

def check_predictions_binary(model, pred_path, gen_func, add_func, add_text, mapping, rotate=False):
    test_im, test_labels = gen_func(add_func, pred_path, rotate)
    _, accuracy = model.evaluate(test_im, test_labels)
    acc = (accuracy * 100)

    predictions = model.predict_classes(test_im)

    predictions_list  = predictions.flatten()
    predictions_list = predictions_list.tolist()
    true_list = test_labels.tolist()
    for l in [predictions_list, true_list]:
        for i in range(len(l)):
            l[i] = mapping[l[i]]

    mat = confusion_matrix(true_list, predictions_list, labels=list(mapping.values()))
    df_cm = pd.DataFrame(mat, columns=np.unique(true_list), index=np.unique(true_list))
    df_cm.index.name = 'Actual'
    df_cm.columns.name = 'Predicted'
    plt.figure(figsize=(8, 5))
    sn.set(font_scale=1.1)
    sn.heatmap(df_cm, cmap='Blues', annot=True, annot_kws={"size": 12}, cbar=False)
    plt.title(add_text+ ", accuracy: "+str(acc))
    plt.savefig(add_text+".png")
    plt.clf()