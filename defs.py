#common definitions
names = ['aut', 'spring', 'summer', 'winter']

IMAGE_WIDTH = 145
IMAGE_HEIGHT = 80
IMAGE_CHANNELS = 3

names_ar = ['cloudy', 'rain', 'shine', 'sunrise']

common_path = '/home/alona/PycharmProjects/finalll/'

paths_lumi = [common_path+'data_lumi/class_bright', common_path+'data_lumi/class_muted']
paths_temp = [common_path+'data_temp/class_cold', common_path+'data_temp/class_warm']

paths_lumi_test = [common_path+'data_lumi_test/class_bright', common_path+'data_lumi_test/class_muted']
paths_temp_test = [common_path+'data_temp_test/class_cold', common_path+'data_temp_test/class_warm']
